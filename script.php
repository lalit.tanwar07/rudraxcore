<?php
function combine_files($list, $mime) {

	if (!is_array($list))
		throw new Exception("Invalid list parameter");

	ob_start();

	$lastmod = filemtime(__FILE__);

	foreach ($list as $fname) {
		$fm = @filemtime($fname);

		if ($fm === false) {
			$msg = $_SERVER["SCRIPT_NAME"].": Failed to load file '$fname'";
			if ($mime == "application/x-javascript") {
				echo 'alert("'.addcslashes($msg, "\0..\37\"\\").'");';
				exit(1);
			} else {
				die("*** ERROR: $msg");
			}
		}

		if ($fm > $lastmod)
			$lastmod = $fm;
	}

	//--

	$if_modified_since = preg_replace('/;.*$/', '',
			$_SERVER["HTTP_IF_MODIFIED_SINCE"]);


	$gmdate_mod = gmdate('D, d M Y H:i:s', $lastmod) . ' GMT';
	$etag = '"'.md5($gmdate_mod).'"';

	if (headers_sent())
		die("ABORTING - headers already sent");

	if (($if_modified_since == $gmdate_mod) or
			($etag == $_SERVER["HTTP_IF_NONE_MATCH"])) {
		if (php_sapi_name()=='CGI') {
			Header("Status: 304 Not Modified");
		} else {
			Header("HTTP/1.0 304 Not Modified");
		}
		exit();
	}
	header("Last-Modified: $gmdate_mod");
	header("ETag: $etag");

	fc_enable_gzip();

	// Cache-Control
	$maxage = 30*24*60*60;   // 30 Tage (Versions-Unterstützung im HTML Code!)

	$expire = gmdate('D, d M Y H:i:s', time() + $maxage) . ' GMT';
	header("Expires: $expire");
	header("Cache-Control: max-age=$maxage, must-revalidate");

	header("Content-Type: $mime");

	echo "/* ".date("r")." */\n";
	foreach ($list as $fname) {
		echo "\n\n/***** $fname *****/\n\n";
		readfile($fname);
	}
}


function files_hash($list, $basedir="") {
	$temp = array();
	$incomplete = false;

	if (!is_array($list))
		$list = array($list);

	if ($basedir!="")
		$basedir="$basedir/";

	foreach ($list as $fname) {
		$t = @filemtime($basedir.$fname);
		if ($t===false)
			$incomplete = true;
		else
			$temp[] = $t;
	}

	if (!count($temp))
		return "ERROR";

	return md5(implode(",",$temp)) . ($incomplete ? "-INCOMPLETE" : "");
}


function fc_compress_output_gzip($output) {
	$compressed = gzencode($output);

	$olen = strlen($output);
	$clen = strlen($compressed);

	if ($olen)
		header("X-Compression-Info: original $olen bytes, gzipped $clen bytes ".
				'('.round(100/$olen*$clen).'%)');

	return $compressed;
}
function fc_compress_output_deflate($output) {

	$compressed = gzdeflate($output, 9);

	$olen = strlen($output);
	$clen = strlen($compressed);

	if ($olen)
		header("X-Compression-Info: original $olen bytes, deflated $clen bytes ".
				'('.round(100/$olen*$clen).'%)');

	return $compressed;

}

function fc_enable_gzip() {
	if(isset($_SERVER['HTTP_ACCEPT_ENCODING']))
		$AE = $_SERVER['HTTP_ACCEPT_ENCODING'];
	else
		$AE = $_SERVER['HTTP_TE'];
	$support_gzip = !(strpos($AE, 'gzip')===FALSE);
	$support_deflate = !(strpos($AE, 'deflate')===FALSE);
	if($support_gzip && $support_deflate) {
		$support_deflate = $PREFER_DEFLATE;
	}
	if ($support_deflate) {
		header("Content-Encoding: deflate");
		ob_start("fc_compress_output_deflate");
	} else{
		if($support_gzip){
			header("Content-Encoding: gzip");
			ob_start("fc_compress_output_gzip");
		} else{
			ob_start();
		}
	}
}

echo  "---".$_REQUEST['f'];
print_r(combine_files(explode(";",$_REQUEST['f']),"application/javascript"));